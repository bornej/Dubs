#!/bin/sh
sources=./unprocessed_sources
sources_flacs=./sources_flacs
for dir in $sources/*
do
    #echo $dir
    source_dir_basename=$(basename $dir)
    mkdir -p $sources_flacs/$source_dir_basename
    for source in $dir/*
    do
        # get the file "source" prefix
        echo $source
        # remove spaces from file name
        [ -f "$source" ] && mv "$source" "`echo $source|tr '[:space:]' '-'`"
    done
    for source in $dir/*
    do
        source_basename=$(echo "$source" | sed -r "s/.+\/(.+)\..+/\1/")
        echo $sources_flacs/$source_dir_basename/$source_basename.flac
        flac $source -o $sources_flacs/$source_dir_basename/$source_basename.flac
    done
done
