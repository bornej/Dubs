---
title: "Live dub"
duration: "4:26"
artist: "Borne Jonathan"
album: "Dubs"
year: "2017"
genre: "Dub"
copyright: "Live dub Copyright (C) 2017 Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Dubs/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Live_dub"
---
