---
title: "Shinda"
duration: "6:08"
artist: "Borne Jonathan"
album: "Dubs"
genre: "Dub"
copyright: "Shinda Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Dubs/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Shinda"
---
