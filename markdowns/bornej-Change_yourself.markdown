---
title: "Change yourself"
duration: "1:42"
artist: "Borne Jonathan"
album: "Dubs"
genre: "Dub"
copyright: "Change yourself Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Dubs/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Change_yourself"
---
